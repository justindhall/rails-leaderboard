class CreateLeaderboards < ActiveRecord::Migration[5.2]
  def change
    create_table :leaderboards do |t|
      t.string :name
      t.string :img
      t.integer :wins

      t.timestamps
    end
  end
end
