class LeaderboardsController < ActiveRecordApi::Rest::Controller
  protect_from_forgery
  def handle_unverified_request
    true
  end
end
