json.extract! leaderboard, :id, :name, :img, :string, :wins, :created_at, :updated_at
json.url leaderboard_url(leaderboard, format: :json)
